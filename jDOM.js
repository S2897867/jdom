function jDOM(newPointer,newHtmlTag,newClassId,newId) {
    newPointer || (newPointer = null)
    newHtmlTag || (htmlTag = "div")
    newClassId || (newClassId = newPointer)
    newId || (newId = "")
    this.class = newClassId;
    this.id = newId;
    this.htmlTag = newHtmlTag;
    this.innerHTML = "";
    this.objectPointer = newPointer;
    this.events = {
    };
    this.style={
    };
    this.attribute={
    };
    this.children={
        "length" : 0,
    };
}
jDOM.prototype.on = function (eventType,functionName){
    this.events[""+eventType] = functionName;
}

jDOM.prototype.addChild = function (child){
    this.children[this.children.length]=child;
    this.children.length++;
}


 jDOM.prototype.toHTML = function (){
    var html = "";
    html +=`<`+this.htmlTag+` `;
    //add id
    html+= ` id="`+this.id+`" `;
    html+= ` class="`+this.class+`" `;
    //add class

    //add events
    for ( var jason in this.events){
        var tempEvent = ` `+jason+`="`;
            tempEvent+=this.objectPointer+`.`+this.events[jason]+`" `;
        html+=tempEvent;
    }
    //add style
    var style =`style="`;
        for ( var jason in this.style){
            style+=` `+jason+`:`+ this.style[jason]+`;`;
        }
        style+=`"`;
    html +=style;
    //add attributes
        for ( var jason in this.attribute){
            html+= ` `+jason+`="`+this.attribute[jason]+`" `;
        }

    html += ` >`
    //add inner html
    html+=this.innerHTML;
    //add children
    for (var i = 0; i<this.children.length; i++){
        html += this.children[i].toHTML();
    }
    html +=` </`+this.htmlTag+`>`;
    return html;
}



jDOM.prototype.onPage = function (){
    var element = document.getElementById(this.id);
    if (typeof(element)!='undefined' && element != null){
        return true;
    }else{
        console.log("element with id'"+this.id+"' is null");
        return false;
    }
}


jDOM.prototype.styler = function (style, propertie){
    if (this.onPage()){
        document.getElementById(this.id).style[this.styleInlineToJs(style)+""] = propertie;
    }
    this.style[this.styleJsToInline(style)] = propertie;
}

jDOM.prototype.getElementById = function (id){
    if (temp.onPage()){
    return document.getElementById(id);
    }else{
        return null;
    }
}

jDOM.prototype.getChildById = function (id){
    for (var i = 0; i<this.children.length; i++){
        if (this.children[i].id == id){
            return this.children[i];
        }else{
            var temp = this.children[i].getChildById(id);
            if (temp != null ){
                return temp;
            }
        }
    }
    return null;
}

//demo
jDOM.prototype.newFunction = function (){
    alert("working");
}









//private functions

jDOM.prototype.styleJsToInline = function  (styleString){//private function
    styleString = styleString.replace('A',"-a");
    styleString = styleString.replace('B',"-b");
    styleString = styleString.replace('C',"-c");
    styleString = styleString.replace('D',"-d");
    styleString = styleString.replace('E',"-e");
    styleString = styleString.replace('F',"-f");
    styleString = styleString.replace('G',"-g");
    styleString = styleString.replace('H',"-h");
    styleString = styleString.replace('I',"-i");
    styleString = styleString.replace('J',"-j");
    styleString = styleString.replace('K',"-k");
    styleString = styleString.replace('L',"-l");
    styleString = styleString.replace('M',"-m");
    styleString = styleString.replace('N',"-n");
    styleString = styleString.replace('O',"o");
    styleString = styleString.replace('P',"-p");
    styleString = styleString.replace('Q',"-q");
    styleString = styleString.replace('R',"-r");
    styleString = styleString.replace('S',"-s");
    styleString = styleString.replace('T',"-t");
    styleString = styleString.replace('U',"-u");
    styleString = styleString.replace('V',"-v");
    styleString = styleString.replace('W',"-w");
    styleString = styleString.replace('X',"-x");
    styleString = styleString.replace('Y',"-y");
    styleString = styleString.replace('Z',"-z");
    return styleString;
}
jDOM.prototype.styleInlineToJs = function  (styleString){//private function
    styleString = styleString.replace('-a',"A");
    styleString = styleString.replace('-b',"B");
    styleString = styleString.replace('-c',"C");
    styleString = styleString.replace('-d',"D");
    styleString = styleString.replace('-e',"E");
    styleString = styleString.replace('-f',"F");
    styleString = styleString.replace('-g',"G");
    styleString = styleString.replace('-h',"H");
    styleString = styleString.replace('-i',"I");
    styleString = styleString.replace('-j',"J");
    styleString = styleString.replace('-k',"K");
    styleString = styleString.replace('-l',"L");
    styleString = styleString.replace('-m',"M");
    styleString = styleString.replace('-n',"N");
    styleString = styleString.replace('-o',"O");
    styleString = styleString.replace('-p',"P");
    styleString = styleString.replace('-q',"Q");
    styleString = styleString.replace('-r',"R");
    styleString = styleString.replace('-s',"S");
    styleString = styleString.replace('-t',"T");
    styleString = styleString.replace('-u',"U");
    styleString = styleString.replace('-v',"V");
    styleString = styleString.replace('-w',"W");
    styleString = styleString.replace('-x',"X");
    styleString = styleString.replace('-y',"Y");
    styleString = styleString.replace('-z',"Z");
    return styleString;
}
