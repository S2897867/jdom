# README #

jDOM is an js object library that standardises attaching JavaScript objects to HTML DOM. It allows you to create a HTML element with JavaScript (even nested elements !) and then add attributes, inline styling, class/id names and event handlers. 


### How do I get set up? ###

## Summary of set up ##
1. You need to download the latest master version of jDOM. This can be found [here](https://bitbucket.org/S2897867/jdom/downloads)
2. Extract the zip file and copy the jDOM.js into the directory you are storing your websites JavaScript files.
3. Then include the jDOM.js file in your html or PHP document. Example:
```
#!html
<script src="jDOM.js" type="text/javascript"></script>
```

### Contribution guidelines ###
This is currently the vanilla version of jDOM it dose not normally except out side modifications. You may suggest modifications or solutions to bugs but it is up to the copyright holder to decided if external changes or fixes are added to the core code.   


## Licence Overview ##
Copyright (C) 2016  Zachary Radloff

This jDOM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

jDOM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).

### Who do I talk to? ###
If you have any questions get in touch with me my BitBucket user name is S2897867.
Found a bug? [ost it on jDOM's official[ BitBucket error tracker ](https://bitbucket.org/S2897867/jdom/issues?status=new&status=open).